package models

/**
 * Created by wnow on 07.05.17.
 */

case class RegisteredUser(id: Int, userId: String, provider: String, firstName: String, lastName: String, fullName: String, email: String, avatarImage: String)

case class Products(id: Long, name: String, price: Double, description: String, catId: Long)

case class Categories(id: Long, name: String)

case class Purchases(id: Long, idprod: String)