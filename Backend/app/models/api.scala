package models

/**
 * Created by wnow on 07.05.17.
 */

import play.api.libs.json.{ Json, OFormat }

case class ProductsREST(id: Long, name: String, price: Double, description: String, catId: Long)

object ProductsREST {
  implicit val productsFormat = Json.format[ProductsREST]
}

case class CategoriesREST(id: Long, name: String)

object CategoriesREST {
  implicit val categoriesFormat = Json.format[CategoriesREST]
}

case class PurchasesREST(idprod: String)

object PurchasesREST {
  implicit val purchasesFormat = Json.format[PurchasesREST]
}

case class RegisteredUserREST(id: Int, userId: String, provider: String, firstName: String, lastName: String, fullName: String, email: String, avatarImage: String)

object RegisteredUserREST {
  implicit val productFormat: OFormat[RegisteredUserREST] = Json.format[RegisteredUserREST]
}

