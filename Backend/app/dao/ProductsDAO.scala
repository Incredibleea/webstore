package dao

/**
 * Created by wnow on 07.05.17.
 */

import javax.inject.Inject

import models.{ Products, ProductsREST }
import play.api.db.slick.{ DatabaseConfigProvider, HasDatabaseConfigProvider }
import slick.driver.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ ExecutionContext, Future }

case class ProductsDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)
    extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  class ProductsTable(tag: Tag) extends Table[Products](tag, "PRODUCTS") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def price = column[Double]("price")
    def description = column[String]("description")
    def catId = column[Long]("catId")

    def * = (id, name, price, description, catId) <> (models.Products.tupled, models.Products.unapply)
  }

  private val products = TableQuery[ProductsTable]

  def all(implicit ec: ExecutionContext): Future[List[ProductsREST]] = {
    val query = products
    val results = query.result
    val futureProducts = db.run(results)
    futureProducts.map(
      _.map {
      a => ProductsREST(id = a.id, name = a.name, price = a.price, description = a.description, catId = a.catId)
    }.toList
    )
  }
}