package dao

/**
 * Created by wnow on 07.05.17.
 */

import javax.inject.Inject

import models.{ Purchases, PurchasesREST }
import play.api.db.slick.{ DatabaseConfigProvider, HasDatabaseConfigProvider }
import slick.driver.JdbcProfile
import slick.lifted.ProvenShape
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ ExecutionContext, Future }

class PurchasesDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)
    extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val purchases = TableQuery[PurchasesTable]

  def all(implicit ec: ExecutionContext): Future[List[PurchasesREST]] = {
    val query = purchases
    val results = query.result
    val futurePurchases = db.run(results)
    futurePurchases.map(
      _.map {
      a => PurchasesREST(idprod = a.idprod)
    }.toList
    )
  }

  //def insert(purchase: Purchases): Future[Unit] = db.run(purchases += purchase).map(_ => ())

  def insert(purchase: Purchases): Future[PurchasesREST] = {

    val insertQuery = purchases returning purchases.map(_.id) into ((purchase, id) => purchase.copy(id = id))
    val action = insertQuery += purchase
    val dbAc = db.run(action)
    dbAc.map(a => PurchasesREST(idprod = a.idprod))
  }

  class PurchasesTable(tag: Tag) extends Table[Purchases](tag, "PURCHASES") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def idprod = column[String]("idprod")

    def * = (id, idprod) <> (models.Purchases.tupled, models.Purchases.unapply)
  }
}