package controllers

import javax.inject.Inject

import dao.RegisteredUserDAO
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
class UserController @Inject() (registeredUserDAO: RegisteredUserDAO) extends Controller {

  def getAll = Action.async { implicit request =>
    registeredUserDAO.all map {
      categories => Ok(Json.toJson(categories))
    }
  }
  def getByUserId(userId: String) = Action.async {
    implicit request =>
      registeredUserDAO.getByUserId(userId) map {
        usr => Ok(Json.toJson(usr))
      }
  }
}
