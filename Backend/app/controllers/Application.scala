package controllers

import javax.inject.Inject

import dao.{ CategoriesDAO, ProductsDAO, PurchasesDAO }
import models.{ Purchases, PurchasesREST }
import play.api.libs.json.Json
import play.api.mvc._

import play.api.libs.concurrent.Execution.Implicits.defaultContext

class Application @Inject() (productsDAO: ProductsDAO, categoriesDAO: CategoriesDAO, purchasesDAO: PurchasesDAO)
    extends Controller {

  def products = Action.async { implicit request =>
    {
      productsDAO.all map {
        products => Ok(Json.toJson(products))
      }
    }
  }

  def categories = Action.async { implicit request =>
    {
      categoriesDAO.all map {
        categories => Ok(Json.toJson(categories))
      }
    }
  }

  def purchases = Action.async { implicit request =>
    {
      purchasesDAO.all map {
        purchases => Ok(Json.toJson(purchases))
      }
    }
  }

  def addPurchase = Action.async { implicit request =>
    var json: PurchasesREST = request.body.asJson.get.as[PurchasesREST]
    var purchase = Purchases(id = 0, idprod = json.idprod)
    var purchaseResult = purchasesDAO.insert(purchase)
    purchaseResult.map {
      pur => Ok(Json.toJson(pur))
    }
  }
}
