import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product/product.service';
import { Product } from '../product/product';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Category} from "../product/category";
import {Purchase} from "../product/purchase";

@Component({
  selector: 'navbar',
  templateUrl: './nav.component.html'
})
export class NavComponent {

  products: Product[];
  categories: Category[];
  purchases: Purchase[];

  productForm: FormGroup;

  constructor(private productService: ProductService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.productService.getProducts().subscribe(data => this.products = data);

    this.productService.getCategories().subscribe(data => this.categories = data);

    this.productService.getPurchases().subscribe(data => this.purchases = data);


    console.log(this.route.snapshot.params);
  }
}
