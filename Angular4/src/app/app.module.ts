import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { RouterModule } from '@angular/router';
import { ProductService } from './product/product.service';
import { ProductForm } from './product/product_form.component';
import {NavComponent} from "./nav/nav.component";
import {FooterComponent} from "./footer/footer.component";
import {LoginComponent} from "./login/login.component";
import { CookieService } from 'angular2-cookie/services/cookies.service';


@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    NavComponent,
    FooterComponent,
    ProductForm,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: LoginComponent },
      { path: 'products', component: ProductComponent },
      { path: 'moreparams/:tytul/:opis', component: ProductComponent },
      { path: 'addproduct', component: ProductForm }
      ])
  ],
  providers: [ ProductService, CookieService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
