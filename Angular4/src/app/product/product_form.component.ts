import { Component } from '@angular/core';
import { ProductService } from './product.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Product } from './product';

 @Component({
   selector: 'product-form',
   templateUrl: './product_form.component.html',
   styleUrls: ['./product_form.component.css']
 })

 export class ProductForm {
     productForm: FormGroup;

     id: number;
     name: string;
     price: string;
     description: string;
     catId: number;

     constructor(private productService: ProductService) {
         //this.productService.getProducts();

         this.productForm = new FormGroup({
           id: new FormControl('id', Validators.required),
           name: new FormControl('name', Validators.required),
           price: new FormControl('price', Validators.required),
           description: new FormControl('description', Validators.required),
           catId: new FormControl('catId', Validators.required)
         });
     }

     addProduct(event) {
       console.log(event);
       console.log(this.productForm.value);
       this.productService.sendToPlay(this.productForm.value);
     }
 }
