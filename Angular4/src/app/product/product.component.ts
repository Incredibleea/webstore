import { Component, OnInit } from '@angular/core';
import { ProductService } from './product.service';
import { Product } from './product';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Category } from "./category";
import { Purchase } from "./purchase";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products: Product[];
  categories: Category[];
  purchases: Purchase[];

  productForm: FormGroup;

  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.productService.getProducts().subscribe(data => this.products = data);

    this.productService.getCategories().subscribe(data => this.categories = data);

    this.productService.getPurchases().subscribe(data => this.purchases = data);

    console.log(this.route.snapshot.params);
  }

  addPurchase(prod: Product) {
    let pur: Purchase = new Purchase;
    pur.idprod = prod.name;
    this.productService.sendToPlay(pur);
  }

 /* getProductsNumber() {
    return this.products.length
  }
*/
 /* addProduct(event) {
    console.log(event);
    console.log(this.productForm.value);
    this.productService.sendToPlay(this.productForm.value);
  }*/

}
